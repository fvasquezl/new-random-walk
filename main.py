from turtle import Turtle, Screen, colormode
from random import random, randint, choice

angles = (0, 60, 200, 300)
step_size = 30
steps = 100
colormode(255)
t = Turtle()
t.pensize(10)
t.speed(2)
for _ in range(steps):
    t.pencolor(randint(0, 255), randint(0, 255), randint(0, 255))
    t.setheading(choice(angles))
    t.forward(step_size)

screen = Screen()
screen.exitonclick()
